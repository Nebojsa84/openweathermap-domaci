$(document).ready(function() {
  var apiKey = "3726fe9cef4ab9d506ee77d8d8f7f3d1";
  var rest_url = "https://api.openweathermap.org/data/2.5/weather?";
  var units = "metric";

  $("#btn-name").click(function() {
    var radiobtn = $(":radio:checked").val();
    var inpt = $("#findByName").val();

    if (radiobtn == "Name") {
      var requestUrl =
        rest_url + "q=" + inpt + "&units=" + units + "&appid=" + apiKey;
    }
    if (radiobtn == "Id") {
      var requestUrl =
        rest_url + "id=" + inpt + "&units=" + units + "&appid=" + apiKey;
    }
    if (radiobtn == "Zip") {
      var code = $("#zip").val();
      var requestUrl =
        rest_url +"zip=" +inpt +"," +code +"&units=" +units +"&appid=" +apiKey;
    }

    $.get(requestUrl, showData);
  });

  $(":radio").change(function() {
    if ($(":radio:checked").val() == "Zip") {
      var zipInput =
        "<input id='zip' placeholder='Enter country code'  type='text'/>";
      $("#search").append(zipInput);
    }
  });

  function showData(data, status) {
    var $container = $("#tb");
    $container.empty();

    var temperature = data.main.temp;
    var min_temp = data.main.temp_min;
    var max_temp = data.main.temp_max;
    var city_id = data.id;
    var c_name = data.name;
    var country = data.sys.country;
    var pressure = data.main.pressure;
    var humidity = data.main.humidity;

    var table =
      "<table class='table table-bordered'>" +
      "<tr>" +
      "<th>City name</th>" +
      "<th>City id</th>" +
      "<th>Country</th>" +
      "<th>Temperature</th>" +
      "<th>Max temperature</th>" +
      "<th>Min temperature</th>" +
      "<th>Humidity</th>" +
      "<th>Pressure</th>" +
      "</tr>"+
      "<tr>" +
      "<td>" + c_name + "</td>" +
      "<td>" + city_id +"</td>" +
      "<td>" + country +"</td>" +
      "<td>" + temperature +"</td>" +
      "<td>" + max_temp +"</td>" +
      "<td>" + min_temp +"</td>" +
      "<td>" + pressure +"</td>" +
      "<td>" + humidity +"</td>" +
      "<tr>" +
      "</table>";

    var div = $("<div></div>");
    var h1 = $("<h1></h1>");
    var weather = $("<p></p>");

    weather.attr("id", "weather");
    h1.text("City: " + data.name);
    h1.attr("id", "city");
    div.append(h1, "<br />", weather, table);
    $container.append(div);
  }
});
